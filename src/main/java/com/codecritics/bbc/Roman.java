package com.codecritics.bbc;

public class Roman implements RomanNumeralGenerator {
    static String decimalNumber(int n, char one, char five, char ten) {
        String s;
        switch (n) {
            case 0: s = ""; break;
            case 1: s = "" + one; break;
            case 2: s = "" + one + one; break;
            case 3: s = "" + one + one + one; break;
            case 4: s = "" + one + five; break;
            case 5: s = "" + five; break;
            case 6: s = "" + five + one; break;
            case 7: s = "" + five + one + one; break;
            case 8: s = "" + five + one + one + one; break;
            case 9: s = "" + one + ten; break;
            default: s = "???";
        }
        return s;
    }
    public  String generate(int number) {
        int u = number % 10;
        int d = (number / 10) % 10;
        int c = (number / 100) % 10;
        int m = (number / 1000) % 1000;
        return (decimalNumber(m, 'M', '?','?') +
                decimalNumber(c, 'C', 'D','M') +
                decimalNumber(d, 'X', 'L','C') +
                decimalNumber(u, 'I', 'V','X'));
    }
}
