package com.codecritics.bbc;

import java.util.Scanner;

/**
 * Hello world!
 */
public class Solution {

    public static void main(String[] args) {
        System.out.println("Please give the number:");
        Scanner sc = new Scanner(System.in);
        int number  = sc.nextInt();
        Roman roman = new Roman();
        System.out.println(number + ": " + roman.generate(number));
    }

}
