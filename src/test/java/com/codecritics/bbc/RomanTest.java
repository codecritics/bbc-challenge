package com.codecritics.bbc;

import static org.junit.Assert.assertTrue;

import junit.framework.TestCase;
import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class RomanTest extends TestCase {
    /**
     * Rigorous Test :-)
     */

    @Test
    public void testOne() throws AssertionError {
        Roman roman = new Roman();
        try {
            assertEquals("I", roman.generate(1));
            System.out.println("1 = I;");
        } catch (AssertionError e) {
            System.out.println("1 != I");
            throw e;
        }
    }

    @Test
    public void testFive() throws AssertionError {
        Roman roman = new Roman();
        try {
            assertEquals("V", roman.generate(5));
            System.out.println("5 = V;");
        } catch (AssertionError e) {
            System.out.println("5 != V");
            throw e;
        }
    }

    @Test
    public void testTen() throws AssertionError {
        Roman roman = new Roman();
        try {
            assertEquals("X", roman.generate(10));
            System.out.println("10 = X;");
        } catch (AssertionError e) {
            System.out.println("10 != X");
            throw e;
        }
    }

    @Test
    public void testTwenty() throws AssertionError {
        Roman roman = new Roman();
        try {
            assertEquals("XX", roman.generate(20));
            System.out.println("20 = XX;");
        } catch (AssertionError e) {
            System.out.println("20 != XX");
            throw e;
        }
    }

    @Test
    public void testLargeNumber() throws AssertionError {
        Roman roman = new Roman();
        try {
            assertEquals("MMMCMXCIX", roman.generate(3999));
            System.out.println("3999 = MMMCMXCIX;");
        } catch (AssertionError e) {
            System.out.println("3999 != MMMCMXCIX");
            throw e;
        }
    }
}
