# BBC Coding Kata - Roman Numerals

This project is my solution proposed to implement the Roman Numeral Generator.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

You'll need :
* 5 min of your precious time.
* Java
* Maven

### Prerequisites

Maven and Java 8

### Installing

A step by step series of examples that tell you have to get a development env running

Say what the step will be:

``` sh
mvn install
```
Build the Jar:
``` sh
mvn compile
```
Produce the jar file in /target and execute the unit tests:
``` sh
mvn clean package
```
Launch the jar file
``` sh
 java -jar target/bbc-challenge-1.0-SNAPSHOT.jar
```
<!---
And repeat

```
until finished
```

End with an example of getting some data out of the system or using it for a little demo
--->
## Running the tests

``` sh
mvn clean package
```
<!---
Explain how to run the automated tests for this system

### Break down into end to end tests

Explain what these tests test and why

```
Give an example
```
-->
## Approach of the solution

The solution proposed to this problem was not immediate,
I tried to simplify it and progress through
steps.

Let's note now that we will only look for
deal with numbers between 1 and 3999.

A first simplification was to start with
additive notation, which takes into account only the
first rule (that is, we will write, for example,
VIIII for 9). Once it is finalized, it will be
time to go to the true Roman writing by adding
the rule of subtractive.

#### Additive version of decimalNumber

The method *decimalNumber* gives us the correct V and I, if we call it on number N. 
``` java
static String decimalNumber (int n) {
    String s = "";
    if (n >= 5) s = s + "V";
    switch (n % 5) {
        case 1: s = s + "I"; break;
        case 2: s = s + "II"; break;
        case 3: s = s + "III"; break;
        case 4: s = s + "IIII"; break;
    }
    return s;
}
```
Then I added two additional parameters to the function so it can take letters like "X and L" or "C and D"
``` java
static String decimalNumber (int x, char i, char v) {
    String s = "";
    if (x >= 5) s = s + v;
    switch (x % 5) {
        case 1: s = s + i; break;
        case 2: s = s + i + i; break;
        case 3: s = s + i + i + i; break;
        case 4: s = s + i + i + i + i; break;
    }
    return s;
}
```

#### Subtractive and Additive version of decimalNumber

``` java
static String decimalNumber(int n, char one, char five, char ten) {
    String s;
    switch (n) {
        case 0: s = ""; break;
        case 1: s = "" + one; break;
        case 2: s = "" + one + one; break;
        case 3: s = "" + one + one + one; break;
        case 4: s = "" + one + five; break;
        case 5: s = "" + five; break;
        case 6: s = "" + five + one; break;
        case 7: s = "" + five + one + one; break;
        case 8: s = "" + five + one + one + one; break;
        case 9: s = "" + one + ten; break;
        default: s = "???";
    }
    return s;
}
```
## Built With

* [Java 8](https://www.java.com/en/download/faq/release_changes.xml)
* [Junit](https://junit.org/junit5/) 

## Authors

* **Tanawa Tsamo Marius** - *BBC Coding Challenge* - [Codecritics](https://gitlab.com/codecritics/)

